package de.thomas_oster.lazysql;

import de.thomas_oster.lazysql.annotations.LazySQLSelect;
import de.thomas_oster.lazysql.SimpleStuffWithoutDBLazyDb.*;
import de.thomas_oster.lazysql.annotations.LazySQLExec;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Thomas Oster <thomas.oster@upstart-it.de>
 */
public class SimpleStuffWithoutDB {
    
    private Connection con;
    private SimpleStuffWithoutDBLazyDb db;
    
    @LazySQLSelect(
        value = "SELECT * FROM bla",
        returns = "String blubb"
    )
    public List<String> testSingleColumnResult() throws SQLException {
        return db.testSingleColumnResult(con);
    }
    
    @LazySQLSelect(
        value = "SELECT * FROM bla",
        returns = "String blubb, Integer bla",
        returnFirstOrNull = true
    )
    public TestSingleRowResultResult testSingleRowResult() throws SQLException {
        return db.testSingleRowResult(con);
    }
    
    @LazySQLSelect(
        value = "SELECT * FROM bla",
        returns = "Integer time",
        returnFirstOrNull = true
    )
    public Integer testSingleRowAndColResult() throws SQLException {
        return db.testSingleRowAndColResult(con);
    }
    
    @LazySQLExec(
        value = "file://queryAsSqlFile.sql"
    )
    public void useQueryFile(String someParam) throws SQLException {
        db.useQueryFile(con, someParam);
    }
    
    @LazySQLSelect(
        value = "SELECT * FROM a WHERE x = :myFirst AND y = :mySecond",
        params = "String myFirst, Integer mySecond",
        returns = "String test",
        useObjectAsInput = true
    )
    public void useOtherParams(String unused) {
        
    }
    
    @LazySQLSelect(
        value = "SELECT * FROM a WHERE x = :myFirst AND y = :mySecond",
        params = "String myFirst, Integer mySecond",
        returns = "String test",
        methodName = "myMethod"
    )
    public void testCustomMethodName(String unused) throws SQLException {
        db.myMethod(con, "bla", 42);
    }
}
